# cursor_indicator

Shows a little colored rectangle near the cursor.

The color can be specified as argument.

I use it to indicate the active layer of my QMK keyboard.

Compiation:

cc cursor_indicator.c -o cursor_indicator  -lX11 -lXext -lXfixes -I/usr/include/freetype2