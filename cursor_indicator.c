#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xft/Xft.h>

// Some variables you can play with :-)
int size = 20;
int line_width = 3;
int x_off = 20;
int y_off = 20;

int main(int argc, char* argv[]) {
    if( argc == 2 ) {
		// Setup display and such
    	char *display_name = getenv("DISPLAY");
    	if (!display_name) {
    	    fprintf(stderr, "%s: cannot connect to X server '%s'\n", argv[0], display_name);
    	    exit(1);
    	}

    	Display *display = XOpenDisplay(display_name);
    	int screen = DefaultScreen(display);

    	// Get the mouse cursor position
    	int win_x, win_y, root_x, root_y, root_x_old, root_y_old = 0;
    	unsigned int mask = 0;
    	Window child_win, root_win;
    	XQueryPointer(display, XRootWindow(display, screen),
    	    &child_win, &root_win,
    	    &root_x, &root_y, &win_x, &win_y, &mask);

    	// Create a window at the mouse position
    	XSetWindowAttributes window_attr;
    	window_attr.override_redirect = 1;
    	Window window = XCreateWindow(display, XRootWindow(display, screen),
    		root_x + x_off, root_y + y_off,   // x, y position
    		size, size,                   // width, height
    	    0,                                  // border width
    	    DefaultDepth(display, screen),      // depth
    	    CopyFromParent,                     // class
    	    DefaultVisual(display, screen),     // visual
    	    CWOverrideRedirect,                 // valuemask
    	    &window_attr                        // attributes
    	);
    	XMapWindow(display, window);
    	XStoreName(display, window, "find-cursor");

    	XClassHint *class = XAllocClassHint();
    	class->res_name = "find-cursor";
    	class->res_class = "find-cursor";
    	XSetClassHint(display, window, class);
    	XFree(class);

    	// Keep the window on top
    	XEvent e;
    	memset(&e, 0, sizeof(e));
    	e.xclient.type = ClientMessage;
    	e.xclient.message_type = XInternAtom(display, "_NET_WM_STATE", False);
    	e.xclient.display = display;
    	e.xclient.window = window;
    	e.xclient.format = 32;
    	e.xclient.data.l[0] = 1;
    	e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_STAYS_ON_TOP", False);
    	XSendEvent(display, XRootWindow(display, screen), False, SubstructureRedirectMask, &e);

    	XRaiseWindow(display, window);
    	XFlush(display);

    	// Prepare to draw on this window
    	XGCValues values;
    	values.graphics_exposures = False;

    	unsigned long valuemask = 0;
    	GC gc = XCreateGC(display, window, valuemask, &values);

    	Colormap colormap = DefaultColormap(display, screen);
    	XColor color;
    	XAllocNamedColor(display, colormap, argv[1], &color, &color);
    	XSetForeground(display, gc, color.pixel);
    	XSetLineAttributes(display, gc, line_width, LineSolid, CapButt, JoinBevel);

    	//// Draw the circle
    	//XDrawArc(display, window, gc,
    	//    line_width/2, line_width/2,   // x, y position
    	//    size - line_width, size - line_width,   // Size
    	//    0, 360 * 64);     // Make it a full circle

		XFillRectangle(display, window, gc, 0, 0, size, size);

		while (True) {
			XQueryPointer(display, XRootWindow(display, screen),
    		    &child_win, &root_win,
    		    &root_x_old, &root_y_old, &win_x, &win_y, &mask);
			if (root_x == root_x_old && root_y == root_y_old) {
				XFillRectangle(display, window, gc, 0, 0, size, size);
        	    XMoveWindow(display, window, root_x_old + x_off, root_y_old + y_off);
        	    XRaiseWindow(display, window);
				XQueryPointer(display, XRootWindow(display, screen),
    			    &child_win, &root_win,
    			    &root_x, &root_y, &win_x, &win_y, &mask);
			}
			root_x = root_x_old;
		   	root_y = root_y_old;
			usleep(800);
    	}
    }
    else if( argc > 2 ) {
       printf("Too many arguments supplied.\n");
    }
    else if (argc !=2) {
       printf("Enter a color, e.g. red, green, blue.\n");
    }
}


/*
 *  The MIT License (MIT)
 * 
 *  from https://unix.stackexchange.com/questions/183910/highlight-current-mouse-position
 *  Copyright © 2015 Martin Tournoij
 *
 *  adapted by qurn to indicate setting
 * 
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  The software is provided "as is", without warranty of any kind, express or
 *  implied, including but not limited to the warranties of merchantability,
 *  fitness for a particular purpose and noninfringement. In no event shall the
 *  authors or copyright holders be liable for any claim, damages or other
 *  liability, whether in an action of contract, tort or otherwise, arising
 *  from, out of or in connection with the software or the use or other dealings
 *  in the software.
 */
